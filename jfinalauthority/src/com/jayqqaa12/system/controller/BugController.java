package com.jayqqaa12.system.controller;

import com.jayqqaa12.UrlConfig;
import com.jayqqaa12.jbase.jfinal.ext.Controller;
import com.jayqqaa12.system.model.Bug;
import com.jayqqaa12.system.validator.BugValidator;
import com.jfinal.aop.Before;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey = "/system/bug",viewPath=UrlConfig.SYSTEM)
public class BugController extends Controller  
{


	public void list()
	{
		
		renderJson(Bug.dao.list(getDataGrid(), getFrom(null)));
	}
	
	

	@Before(value = { BugValidator.class })
	public void add()
	{
		renderJsonResult( getModel(Bug.class).saveAndCreateDate());
		
	}
	@Before(value = { BugValidator.class })
	public void edit()
	{
		renderJsonResult(getModel(Bug.class).updateAndModifyDate());
		
	}
	
	public void delete()
	{
		renderJsonResult(Bug.dao.deleteById(getPara("id")));
	}


}
